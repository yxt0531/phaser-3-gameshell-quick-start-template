# Phaser 3 GameShell Quick Start Template

This is a quick start template for ClockworkPi GameShell using Phaser 3.23.0 and NW.js v0.27.6 for ARMv7.

*Clockwork OS v0.5* or higher is required.

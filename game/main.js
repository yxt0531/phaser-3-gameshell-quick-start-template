var config = {
    type: Phaser.AUTO,
    width: 316,
    height: 236,
    physics: {
        default: "arcade",
        arcade: {
            gravity: { y: 200 },
        },
    },
    scene: [Sample],
};

var game = new Phaser.Game(config);

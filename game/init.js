nw.Window.open("./game/index.html", {}, (win) => {
    var win = nw.Window.get();
    var width = 316;
    var height = 236;

    win.setMaximumSize(width, height);
    win.setMinimumSize(width, height);
    win.resizeTo(width, height);
});

add_shortcut("Escape");

function add_shortcut(key_shortcut) {
    var option = {
        key: key_shortcut,
        active: function () {
            nw.App.quit();
        },
        failed: function (msg) {
            console.log(msg);
        },
    };
    var shortcut = new nw.Shortcut(option);
    nw.App.registerGlobalHotKey(shortcut);
}
